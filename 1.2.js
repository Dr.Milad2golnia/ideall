//2

function getOrder(order) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(order);
      }, Math.random() * 1000);
    });
  }
  
getOrder(1)
.then((order) => {
    console.log('Order 1:', order);
    return getOrder(2);
})
.then((order) => {
    console.log('Order 2:', order);
    return getOrder(3);
})
.then((order) => {
    console.log('Order 3:', order);
})
.catch((error) => {
    console.error('Error:', error);
});
  

/** Output:
* Order 1: 1
* Order 2: 2
* Order 3: 3
 */