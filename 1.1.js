//1
/**
 * OK, Let's start it.
 */

/**
 * First we provide Declerative Implementation:
 */
function transformArrayDeclerative(arr, callback){
    return arr.map(callback);
}

/**
 * And now Imperative Implementation:
 */
function transformArrayImperative(arr, callback){
    let result = []
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        result.push(element);
    }
    return result;
}