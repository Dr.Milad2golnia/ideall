//4

const originalObject = {
    prop1: 'Hello',
    prop2: 'World',
  };
  
const handler = {
    get: function (target, prop) {
        return '404';
    },
};

const proxyObject = new Proxy(originalObject, handler);

console.log(proxyObject.prop1);
console.log(proxyObject.prop2);
  